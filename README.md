Calvary Baptist Church of Burbank is a Bible teaching church with a focus on in-depth, expository, exegetical, teaching by Pastor Anthony Forsyth. Join us for expository Bible teaching at our Burbank Bible Church every Sunday in Burbank, California.

Address: 724 S Glenoaks Blvd, Burbank, CA 91502, USA

Phone: 818-846-6723

Website: https://www.calvarybaptistburbank.org
